$.getJSON('db.json', function (data) {
  let menu = data.menu;

  $.each(menu, function (i, data) {
    $('#daftarMenu').append(
      '<div class="col-sm-4 mb-sm-3"><div class="cards border border-1 p-3 position-relative" style="width: 450px; overflow: hidden"><div class="img float-start"><img src="./image/' +
        data.image +
        '" width="100px" alt="" /><div class="food-name position-absolute bottom-0"><p>' +
        data.kategori +
        '</p></div></div><div class="float-sm-start"><ul class="list-group border-0"><li class="list-group-item border-0"><span>Room</span> :</li><li class="list-group-item border-0"><span>Jumlah</span> :</li><li class="list-group-item border-0"><span>Modifier</span> :</li><li class="list-group-item border-0"><span>Noted</span> :</li></ul></div><div class="list-value float-sm-start"><ul class="list-group border-0"><li class="list-group-item border-0">' +
        data.room +
        '</li><li class="list-group-item border-0">' +
        data.jumlah +
        '</li><li class="list-group-item border-0">' +
        data.modifier +
        '</li><li class="list-group-item border-0">' +
        data.noted +
        '</li></ul></div><div class="status float-lg-end"><ins>Status:</ins><p id="status-' +
        data.id +
        '" class="status-value text-primary">' +
        data.status +
        '</p></div><div class="time fw-bold position-absolute bottom-0 end-0 me-lg-3"><p>' +
        data.waktu +
        '</p></div></div></div></div>'
    );

    let newStatus = localStorage.getItem('myStatus');
    $('#status-' + data.id).text(newStatus);
  });
});

// modal
