const btn = document.querySelector('#btn-yes');
$.getJSON('db.json', function (data) {
  let menu = data.menu;

  $.each(menu, function (i, data) {
    $('#daftarMenu').append(
      '<div class="col-sm-4 mb-sm-3"><div class="cards border border-1 p-3 position-relative" data-bs-toggle="modal" data-bs-target="#exampleModal" style="width: 450px; overflow: hidden"><div class="img float-start"><img src="./image/' +
        data.image +
        '" width="100px" alt="" /><div class="food-name position-absolute bottom-0"><p>' +
        data.kategori +
        '</p></div></div><div class="float-sm-start"><ul class="list-group border-0"><li class="list-group-item border-0"><span>Room</span> :</li><li class="list-group-item border-0"><span>Jumlah</span> :</li><li class="list-group-item border-0"><span>Modifier</span> :</li><li class="list-group-item border-0"><span>Noted</span> :</li></ul></div><div class="list-value float-sm-start"><ul class="list-group border-0"><li class="list-group-item border-0">' +
        data.room +
        '</li><li class="list-group-item border-0">' +
        data.jumlah +
        '</li><li class="list-group-item border-0">' +
        data.modifier +
        '</li><li class="list-group-item border-0">' +
        data.noted +
        '</li></ul></div><div class="status float-lg-end"><ins>Status:</ins><p id="status-' +
        data.id +
        '" class="text-status text-primary">' +
        data.status +
        '</p></div><div class="time fw-bold position-absolute bottom-0 end-0 me-lg-3"><p>' +
        data.waktu +
        '</p></div></div></div></div><div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog modal-dialog-centered p-3"><div class="modal-content p-4 text-center fs-4"><div class="modal-body">Change Order xxx status to "Dibuat"?</div><div class="modal-footer vstack"><button type="button" onclick="ubahStatus(' +
        data.id +
        ')" class="btn btn-primary w-50">YES</button><button type="button" class="btn btn-danger w-50" data-bs-dismiss="modal">NO</button></div></div></div></div>'
    );
  });
});
function ubahStatus(id) {
  let status = 'Dibuat';

  console.log(id);
  localStorage.setItem('myStatus', status);
  window.location.href = 'item-status.html';
}

// modal
